import Api from '@/api'
import { RepositoryState, RepositoryMutations, RepositoryActions, RepositoryGetters, RepositoryMethods } from './base'
import Project from '../models/project'
import { methods as EmployeeRepository } from '@repositories/employee'
import Task from '../models/task'

/**
 * Repository state
 */
const state = RepositoryState()
const mutations = RepositoryMutations({
  ADD_TASK_LIST(state, { uuid, newTaskList }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      if (!project.taskLists.find((tl) => tl.uuid === newTaskList.uuid)) {
        project.taskLists.push(newTaskList)
      }
    }
  },

  UPDATE_TASK_LIST(state, { uuid, updatedTaskList }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      const taskList = project.taskLists.find((taskList) => taskList.uuid === updatedTaskList.uuid)

      if (taskList) {
        for (let key in updatedTaskList) {
          taskList[key] = updatedTaskList[key]
        }
      }
    }
  },

  ASSIGN_TASK_LIST_EMPLOYEE(state, { taskList, employee }) {
    // This is ugly, change later to more prettier solution
    // Also currently allowed to assign one Employee, so we reasign new one to the first elem
    // Should let assign multiple employees, as well as update list of employees
    for (let project of state.items) {
      for (let loopTaskList of project.taskLists) {
        if (loopTaskList.uuid === taskList.uuid) {
          loopTaskList.assignees[0] = EmployeeRepository.findByKey(employee)
        }
      }
    }
  },

  REMOVE_TASK_LIST(state, { uuid, deletedTaskList }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      const taskList = project.taskLists.find((item) => item.uuid === deletedTaskList.uuid)

      if (taskList) {
        project.taskLists = project.taskLists.filter((item) => item.uuid !== taskList.uuid)
      }
    }
  },

  ADD_TASK(state, { uuid, taskListUuid, newTask }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      const taskList = project.taskLists.find((taskList) => taskList.uuid === taskListUuid)

      if (taskList) {
        const task = taskList.tasks.find((task) => task.uuid === newTask.uuid)

        if (!task) {
          taskList.tasks.push(Task.create(newTask))
        }
      }
    }
  },

  UPDATE_TASK(state, { uuid, taskListUuid, updatedTask }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      const taskList = project.taskLists.find((taskList) => taskList.uuid === taskListUuid)

      if (taskList) {
        const task = taskList.tasks.find((task) => task.uuid === updatedTask.uuid)

        if (task) {
          for (let key in updatedTask) {
            task[key] = updatedTask[key]
          }
        }
      }
    }
  },

  TASK_COMPLETED(state, { completedTask, projectUuid, taskListUuid }) {
    const project = state.items.find((project) => project.uuid === projectUuid)

    if (project) {
      let taskList = project.taskLists.find((taskList) => taskList.uuid === taskListUuid)

      if (taskList) {
        let task = taskList.tasks.find((item) => item.uuid === completedTask.uuid)

        if (task) {
          task.isCompleted = !task.isCompleted
        }
      }
    }
  },

  ASSIGN_TASK_EMPLOYEE(state, { task, employee }) {
    // This is ugly, change later to more prettier solution
    // Also currently allowed to assign one Employee, so we reasign new one to the first elem
    // Should let assign multiple employees, as well as update list of employees
    for (let project of state.items) {
      for (let taskList of project.taskLists) {
        for (let loopTask of taskList.tasks) {
          if (loopTask.uuid === task.uuid) {
            loopTask.assignees[0] = EmployeeRepository.findByKey(employee)
          }
        }
      }
    }
  },

  REMOVE_TASK(state, { uuid, deletedTask, taskListUuid }) {
    const project = state.items.find((project) => project.uuid === uuid)

    if (project) {
      let taskList = project.taskLists.find((taskList) => taskList.uuid === taskListUuid)

      if (taskList) {
        let task = taskList.tasks.find((item) => item.uuid === deletedTask.uuid)

        if (task) {
          taskList.tasks = taskList.tasks.filter((item) => item.uuid !== task.uuid)
        }
      }
    }
  }
})
const actions = RepositoryActions(Project, {
  CREATE_TASK_LIST({ commit }, { uuid, data }) {
    return Api.post(`crm/projects/${uuid}/task-lists`, {
      'task-list': data
    }).then((newTaskList) => {
      commit('ADD_TASK_LIST', { uuid, newTaskList })
      return newTaskList
    })
  },

  UPDATE_TASK_LIST({ commit }, { uuid, taskListUuid, data }) {
    return Api.put(`crm/projects/${uuid}/task-lists/${taskListUuid}`, {
      'task-list': data
    }).then((updatedTaskList) => {
      commit('UPDATE_TASK_LIST', { uuid, updatedTaskList })
      return updatedTaskList
    })
  },

  ASSIGN_TASK_LIST_EMPLOYEE({ commit }, { uuid, employee }) {
    return Api.post(`crm/task-list/assign-employee`, {
      uuid: uuid,
      employee: employee
    }).then((taskList) => {
      commit('ASSIGN_TASK_LIST_EMPLOYEE', { taskList, employee })
    })
  },

  REMOVE_TASK_LIST({ commit }, { uuid, taskListUuid }) {
    return Api.delete(`crm/projects/${uuid}/task-lists/${taskListUuid}`)
      .then((deletedTaskList) => {
        commit('REMOVE_TASK_LIST', {uuid, deletedTaskList})

        return deletedTaskList
      })
  },

  CREATE_TASK({ commit }, { uuid, taskListUuid, data }) {
    return Api.post(`crm/projects/${uuid}/task-lists/${taskListUuid}/tasks`, {
      'task': data
    }).then((newTask) => {
      commit('ADD_TASK', { uuid, taskListUuid, newTask })
      return newTask
    })
  },

  UPDATE_TASK({ commit }, { uuid, taskListUuid, taskUuid, data }) {
    return Api.put(`crm/projects/${uuid}/task-lists/${taskListUuid}/tasks/${taskUuid}`, {
      'task': data
    }).then((updatedTask) => {
      commit('UPDATE_TASK', { uuid, taskListUuid, updatedTask })
      return updatedTask
    })
  },

  MARK_TASK_AS_COMPLETED({ commit }, { uuid, projectUuid, taskListUuid, isCompleted }) {
    return Api.post(`crm/task/mark-as-completed`, {
      uuid: uuid,
      is_completed: isCompleted
    }).then((completedTask) => {
      commit('TASK_COMPLETED', {
        completedTask, projectUuid, taskListUuid
      })
    })
  },

  ASSIGN_TASK_EMPLOYEE({ commit }, { uuid, employee }) {
    return Api.post(`crm/task/assign-employee`, {
      uuid: uuid,
      employee: employee
    }).then((task) => {
      commit('ASSIGN_TASK_EMPLOYEE', { task, employee })
    })
  },

  REMOVE_TASK({ commit }, { uuid, taskListUuid, taskUuid }) {
    return Api.delete(`crm/projects/${uuid}/task-lists/${taskListUuid}/tasks/${taskUuid}`)
      .then((deletedTask) => {
        commit('REMOVE_TASK', { uuid, deletedTask, taskListUuid })

        return deletedTask
      })
  }
})
const getters = RepositoryGetters({
  API_NAME: () => 'crm/projects',
  API_RESOURCE_NAME: () => 'project'
})

/**
 * Exports
 */
export default {
  namespaced: true,

  state,
  mutations,
  actions,
  getters
}
export const methods = RepositoryMethods('project')
