export default {
  data() {
    return {
      inputs_props: [
        // column 1
        [
          {
            group_name: 'Default settings',
            inputs: [
              {
                title: 'Language',
                input_group: 'settings',
                input_name: 'locale',
                prop: 'languages_name',
                placeholder: 'Select language',
                type: 'dropdown'
              },
              {
                title: 'Default Currency',
                input_group: 'settings',
                input_name: 'currency_code',
                prop: 'currencies',
                placeholder: 'Select currency',
                type: 'dropdown'
              },
              {
                title: 'Time zone',
                input_group: 'preferences',
                input_name: 'time_zone',
                prop: 'timeZones',
                placeholder: 'Select time zone',
                type: 'dropdown'
              },
              {
                title: 'Date format',
                input_group: 'preferences',
                input_name: 'date_format',
                placeholder: 'Select date format',
                prop: 'dateFormat',
                type: 'dropdown'
              },
              {
                title: 'Rows per page',
                input_group: 'preferences',
                input_name: 'row_per_page',
                prop: 'rowPerPage',
                placeholder: 'Select row per page',
                type: 'dropdown'
              },
              {
                title: 'Financial year start',
                input_group: 'preferences',
                input_name: 'financial_year_start',
                placeholder: 'Financial year start',
                type: 'text'
              }
            ]
          }
        ],
        // column 2
        [
          {
            group_name: 'Accounting',
            inputs: [
              {
                title: 'Accounting',
                input_group: 'module_visibility',
                input_name: 'accounting',
                type: 'switch'
              }
            ]
          },

          {
            group_name: 'AML',
            inputs: [
              {
                title: 'AML',
                input_group: 'module_visibility',
                input_name: 'aml',
                type: 'switch'
              }
            ]
          },

          {
            group_name: 'ID Verification',
            inputs: [
              {
                title: 'ID Verification',
                input_group: 'module_visibility',
                input_name: 'id_verification',
                type: 'switch'
              }
            ]
          },

          {
            group_name: 'Impersonate',
            inputs: [
              {
                title: 'Impersonate',
                input_group: 'module_visibility',
                input_name: 'impersonate',
                type: 'switch'
              }
            ]
          },

          {
            group_name: 'CRM',
            inputs: [
              {
                title: 'CRM',
                input_group: 'module_visibility',
                input_name: 'crm',
                type: 'switch'
              }
            ]
          }
        ],
        // column 3
        [
          {
            group_name: 'Secretarial services',
            inputs: [
              {
                title: 'Business setup',
                input_group: 'module_visibility',
                input_name: 'business_setup',
                type: 'switch'
              },
              {
                title: 'Board minutes',
                input_group: 'module_visibility',
                input_name: 'board_minutes',
                type: 'switch'
              }
            ]
          }
        ]
      ]
    }
  }
}
