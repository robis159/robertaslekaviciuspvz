<?php

namespace App\Domain\Model\CRM\TaskList;

use League\Fractal;
use App\Domain\Model\Authentication\User\UserTransformer;
use App\Domain\Model\CRM\Task\TaskTransformer;
use App\Domain\Model\System\ActivityLog\ActivityTransformer;

class TaskListTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'user',
        'tasks',
        'history'
    ];

    public function excludeForBackup()
    {
        return [
            'user',
            'tasks',
            'history'
        ];
    }

    public function transform(TaskList $taskList)
    {
        return [
            'uuid' => $taskList->uuid,
            'company_uuid' => $taskList->company_uuid,

            'name' => $taskList->name,
            'color' => $taskList->color,

            'assignees' => $taskList->assignees->map(function ($assignee) {
              return $assignee->employee_uuid;
            }),

            'created_at' => $taskList->created_at,
            'updated_at' => $taskList->updated_at,
            'archived_at' => $taskList->archived_at,
            'is_disabled' => $taskList->is_disabled
        ];
    }

    public function includeUser(TaskList $taskList)
    {
        return $this->item($taskList->user, new UserTransformer);
    }

    public function includeTasks(TaskList $taskList)
    {
        return $this->collection($taskList->tasks, new TaskTransformer);
    }

    public function includeHistory(TaskList $taskList)
    {
        return $this->collection($taskList->getHistory(), new ActivityTransformer);
    }
}