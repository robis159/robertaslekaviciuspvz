<?php

namespace App\Domain\Model\CRM\TaskList;

use App\Infrastructure\Persistence\Repository;
use App\Domain\Model\Authentication\Company\Company;
use App\Domain\Model\Documents\Shared\Traits\FillsUserData;
use App\Domain\Model\Documents\Shared\AbstractDocumentRepository;

class TaskListRepository extends AbstractDocumentRepository
{
    use FillsUserData;

    protected $repository;

    public function __construct()
    {
        $this->repository = new Repository(TaskList::class);
    }

    public function saving($taskList, &$data)
    {
        $company = Company::where('account_uuid', auth()->user()->account_uuid)->first();

        $taskList->company_uuid = $company->uuid;
    }
}