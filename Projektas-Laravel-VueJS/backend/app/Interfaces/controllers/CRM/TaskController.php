<?php

namespace App\Interfaces\Http\Controllers\CRM;

use App\Interfaces\Http\Controllers\DocumentController;
use App\Domain\Model\CRM\Task\Task;
use Illuminate\Http\Request;

class TaskController extends DocumentController
{
    public function updateTask(
      $project,
      $taskListUuid,
      $uuid,
      Request $request
    )
    {
      $task = Task::find($uuid);

      if ($task->task_list_uuid === $taskListUuid) {
        $task->update($request->get('task'));
      }

      return $task;
    }

    public function markAsCompleted(Request $request)
    {
      if (!isset($request->uuid)) {
        return;
      }

      $task = Task::find($request->uuid);
      $task->update([
        'is_completed' => $request->is_completed
      ]);

      return $task;
    }

    public function assignEmployee(Request $request)
    {
      if (!isset($request->uuid) && !isset($request->employee)) {
        return;
      }

      $task = Task::find($request->uuid);

      foreach ($task->assignees as $assignee) {
        $assignee->delete();
      }

      $task->assignees()->create([
        'employee_uuid' => $request->employee
      ]);

      return $task;
    }

    public function deleteTask($project, $taskListUuid, $uuid)
    {
      $task = Task::find($uuid);
      $task->delete();

      return $task;
    }

    public function getResourceName()
    {
        return 'task';
    }

    public function getValidationRules($action)
    {
        $rules = [
            static::VALIDATION_RULES_CREATE => [
                $this->getResourceName() => 'required|array',
                "{$this->getResourceName()}.name" => 'required',
            ],
            static::VALIDATION_RULES_PATCH => [
                $this->getResourceName() => 'required|array',
            ]
        ];
        $rules[static::VALIDATION_RULES_UPDATE] = $rules[static::VALIDATION_RULES_CREATE];

        return $rules[$action];
    }

    public function getValidationAttributes()
    {
        return [];
    }
}