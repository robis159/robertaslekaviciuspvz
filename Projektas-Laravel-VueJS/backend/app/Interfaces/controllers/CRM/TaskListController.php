<?php

namespace App\Interfaces\Http\Controllers\CRM;

use App\Interfaces\Http\Controllers\DocumentController;
use App\Domain\Model\CRM\TaskList\TaskList;
use Illuminate\Http\Request;

class TaskListController extends DocumentController
{
    public function updateTaskList($project, $uuid, Request $request)
    {
      $taskList = TaskList::find($uuid);
      $taskList->update($request->get('task-list'));

      return $taskList;
    }

    public function assignEmployee(Request $request)
    {
      if (!isset($request->uuid) && !isset($request->employee)) {
        return;
      }

      $taskList = TaskList::find($request->uuid);

      foreach ($taskList->assignees as $assignee) {
        $assignee->delete();
      }

      $taskList->assignees()->create([
        'employee_uuid' => $request->employee
      ]);

      return $taskList;
    }

    public function deleteTaskList($project, $uuid)
    {
      $taskList = TaskList::find($uuid);
      $taskList->delete();

      return $taskList;
    }

    public function getResourceName()
    {
        return 'task_list';
    }

    public function getValidationRules($action)
    {
        $rules = [
            static::VALIDATION_RULES_CREATE => [
                $this->getResourceName() => 'required|array',
                "{$this->getResourceName()}.client_uuid" => 'nullable|exists:clients,uuid',
                "{$this->getResourceName()}.name" => 'required',
            ],
            static::VALIDATION_RULES_PATCH => [
                $this->getResourceName() => 'required|array',
                "{$this->getResourceName()}.client_uuid" => 'nullable|exists:clients,uuid'
            ]
        ];
        $rules[static::VALIDATION_RULES_UPDATE] = $rules[static::VALIDATION_RULES_CREATE];

        return $rules[$action];
    }

    public function getValidationAttributes()
    {
        return [
            "{$this->getResourceName()}.name" => 'project\'s name',
            "{$this->getResourceName()}.client_uuid" => 'client',
        ];
    }
}