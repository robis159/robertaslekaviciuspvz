<?php
/**
 * CRM
 */
Route::prefix('crm')->group(function () {
    Route::post('projects/{project}/task-lists', 'CRM\ProjectController@storeTaskList');
    Route::put('projects/{project}/task-lists/{uuid}', 'CRM\TaskListController@updateTaskList');
    Route::delete('projects/{project}/task-lists/{uuid}', 'CRM\TaskListController@deleteTaskList');
    Route::post('projects/{project}/task-lists/{taskList}/tasks', 'CRM\ProjectController@storeTask');
    Route::put('projects/{project}/task-lists/{taskList}/tasks/{uuid}', 'CRM\TaskController@updateTask');
    Route::delete('projects/{project}/task-lists/{taskList}/tasks/{task}', 'CRM\TaskController@deleteTask');

    Route::post('task/mark-as-completed', 'CRM\TaskController@markAsCompleted');
    Route::post('task/assign-employee', 'CRM\TaskController@assignEmployee');
    Route::post('task-list/assign-employee', 'CRM\TaskListController@assignEmployee');
});

/**
 * Settings
 */
Route::prefix('settings')->group(function () {
  Route::post('currency', 'Settings\UserSettingsController@changeCurrency');
  Route::post('update', 'Settings\UserSettingsController@updateSetting');
  Route::post('module-visibility', 'Settings\UserSettingsController@moduleVisibility');
});