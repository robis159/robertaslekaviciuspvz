<?php

namespace Kretagra\Http\Controllers\CRM;

use Illuminate\Http\Request;
use Kretagra\Http\Controllers\Controller;

//istrinti
use Kretagra\Models\Supplier;
use Kretagra\Models\WorkState;

use Kretagra\Models\SupplierDb;

class SuppliersDbController extends Controller
{
    private $supplierDb;

    public function __construct(SupplierDb $supplierDb) {
        $this->supplierDb = new $supplierDb;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crm.suppliersDb.list', [
            'suppliersCount' => count($this->supplierDb->all())
        ]);
    }

    /**
     * ajax return all suppliers server side
     * and execute search command if it called
     */
    public function allSuppliers(Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'id',
            2 => 'name',
            3 => 'address',
            4 => 'full_name',
            5 => 'email',
            6 => 'web_address',
            7 => 'products',
            8 => 'products_groups'
        );

        $totalData = $this->supplierDb->count();

        $totalFiltered = $totalData;

        $limit = $request->length;
        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $search = [];
        if (isset($request->searchValues)) {
            $searchValues = $request->searchValues;
            foreach ($searchValues as $key => $value) {
                if (empty($value)) continue;
                if (strpos($value, ',')) {
                    $multiple = explode(',', $value);
                    foreach ($multiple as $str) {
                        array_push($search, [$key, 'LIKE', '%' . trim($str) . '%']);
                    }
                    continue;
                }
                array_push($search, [$key, 'LIKE', '%' . trim($value) . '%']);
            }
        }

        if(count($search) === 0) {
            $suppliers = $this->supplierDb->offSet($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $totalData = $this->supplierDb
                    ->where($search)
                    ->count();

            $suppliers = $this->supplierDb
                    ->where($search)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            $totalFiltered = $totalData;
        }
        $data = array();
        if(!empty($suppliers)) {
            $c = $start + 1;
            foreach($suppliers as $supplier) {
                $edit = route('suppliersDb.edit', $supplier->id);
                $show = route('suppliersDb.show', $supplier->id);

                $nestedData['action'] = "<a href='{$edit}' data-task-id='" . $supplier->id ."' class='btn btn-warning smaller-list btn-id'><i class='fas fa-pencil-alt'></i></a> <a href='$show' class='btn btn-danger smaller-list'><i class='fa fa-trash'></i></a>";
                $nestedData['id'] = $c;
                $nestedData['name'] = $supplier->name;
                $nestedData['address'] = $supplier->address;
                $nestedData['full_name'] = $supplier->full_name;
                $nestedData['email'] = $supplier->email;
                $nestedData['web_address'] = $supplier->web_address;

                $products = json_decode($supplier->products);
                $products = implode(', ', $products);
                $nestedData['products'] = $products;

                $products_groups = json_decode($supplier->products_groups);
                $products_groups = implode(', ', $products_groups);
                $nestedData['products_groups'] = $products_groups;


                $data[] = $nestedData;
                $c++;
            }
        }
        return response()->json([
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.suppliersDb.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->supplierDb->create([
            'name' => $request->name,
            'address' => $request->address,
            'full_name' => $request->full_name,
            'email' => $request->email,
            'web_address' => $request->web_address,
            'products' => json_encode(explode(',', $request->products)),
            'products_groups' => json_encode(explode(',', $request->products_groups)),
        ]);

        return redirect()->route('suppliersDb.index')->with('alert-succes',  'Supplier created');
    }

    /**
     * Show delete window of selected element.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('crm.suppliersDb.delete')->with([
            'supplier' => $this->supplierDb->findOrFail($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('crm.suppliersDb.edit')->with([
            'supplier' => $this->supplierDb->findOrFail($id),
            'states' => WorkState::orderBy('title', 'asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->supplierDb->findOrFail($id)->update([
            'name' => $request->name,
            'address' => $request->address,
            'full_name' => $request->full_name,
            'email' => $request->email,
            'web_address' => $request->web_address,
            'products' => json_encode(explode(',', $request->products)),
            'products_groups' => json_encode(explode(',', $request->products_groups)),
        ]);

        return redirect()->route('suppliersDb.index')->with('alert-warning',  'Supplier updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->supplierDb->findOrFail($id)->delete();

        return redirect()->route('suppliersDb.index')->with('alert-danger',  'Supplier deleted');
    }

    /**
     * copy all email in table
     */
    public function allEmailAddresses(Request $request) {
        \Log::debug('search emails');

        $search = [];
        if (isset($request->searchValues)) {
            $searchValues = $request->searchValues;
            foreach ($searchValues as $key => $value) {
                if (empty($value)) continue;
                array_push($search, [$key, 'LIKE', '%' . $value . '%']);
            }
        }

        if (count($search) === 0) {
            $suppliers = $this->supplierDb->all();
        } else {
            $suppliers= $this->supplierDb
            ->where($search)
            ->get();
        }

        $emailString = '';
        foreach ($suppliers as $supplier) {
                if(empty($supplier['email'])) continue;
                $emailString .= $supplier['email'] . ';';
        }
        \Log::debug('emails: ' . $emailString);
        return response()->json([
            'emails' => $emailString
        ]);
    }
}