@extends('crm.layout.template')

@section('stylesheets')

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Add new supplier</strong>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <a href="{{ route('suppliers.index') }}" class="btn btn-info" style="margin-bottom: 20px;">Go back</a>
                            </div>
                        </div>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                            {!! Form::open(['route' => 'suppliersDb.store', 'method' => 'POST']) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name') !!}
                                        {!! Form::text('name', null, array('class' => 'form-control input-default', 'placeholder' => 'Supplier name', 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', null, array('class' => 'form-control input-default', 'placeholder' => 'Address')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('full_name', 'Full name') !!} 
                                        {!! Form::text('full_name', null, array('class' => 'form-control input-default', 'placeholder' => 'Responsible person full name')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                            {!! Form::label('email', 'Email') !!} 
                                        {!! Form::text('email', null, array('class' => 'form-control input-default', 'placeholder' => 'Supplier email')) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('web_address', 'Web address') !!} 
                                        {!! Form::text('web_address', null, array('class' => 'form-control input-default', 'placeholder' => 'Supplier email')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('products', 'Products') !!} 
                                        {!! Form::text('products', null, array('class' => 'form-control input-default', 'placeholder' => 'Supplier products')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('products_groups', 'Products groups') !!} 
                                        {!! Form::text('products_groups', null, array('class' => 'form-control input-default', 'placeholder' => 'Supplier product groups')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i>
                                        &nbsp;Create
                                    </button>
                                    <a href="{{ route('suppliersDb.index') }}" class="btn btn-info">
                                       Go back
                                    </a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
