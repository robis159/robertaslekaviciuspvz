<?php
    function decodeData($data) {
        $decoded = json_decode($data);
        $decoded = implode(', ', $decoded);
        return $decoded;
    }
?>
@extends('crm.layout.template')

@section('stylesheets')

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"> - Update {{ $supplier->name }}</strong>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <a href="{{ route('suppliers.index') }}" class="btn btn-info" style="margin-bottom: 20px;">Go back</a>
                            </div>
                        </div>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                            {!! Form::open(['route' => ['suppliersDb.update', $supplier->id], 'method' => 'PUT']) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name') !!} 
                                        {!! Form::text('name', $supplier->name, array('class' => 'form-control input-default', 'placeholder' => 'Name')) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', $supplier->address, array('class' => 'form-control input-default', 'placeholder' => 'Address')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('full_name', 'Full name') !!}
                                        {!! Form::text('full_name', $supplier->full_name, array('class' => 'form-control input-default', 'placeholder' => 'Full name')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email') !!}
                                        {!! Form::text('email', $supplier->email, array('class' => 'form-control input-default', 'placeholder' => 'Email')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('web_address', 'Web address') !!}
                                        {!! Form::text('web_address', $supplier->web_address, array('class' => 'form-control input-default', 'placeholder' => 'Web address')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('products', 'Products') !!}
                                        {!! Form::text('products', decodeData($supplier->products), array('class' => 'form-control input-default', 'placeholder' => 'Products')) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('products_groups', 'Products groups') !!}
                                        {!! Form::text('products_groups', decodeData($supplier->products_groups), array('class' => 'form-control input-default', 'placeholder' => 'Products groups')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-warning">
                                        <i class="fa fa-check"></i>
                                        &nbsp;Update
                                    </button>
                                    <a href="{{ route('suppliersDb.index') }}" class="btn btn-info">
                                        Go back
                                    </a>
                                </div>
                            </div>

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
