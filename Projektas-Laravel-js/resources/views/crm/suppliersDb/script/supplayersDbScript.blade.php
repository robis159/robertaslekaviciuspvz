<script>
  jQuery.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      }
  })

  // when page loaded draw table
  let columns = [
      { 'data' : 'action'},
      { 'data' : 'id'},
      { 'data' : 'name'},
      { 'data' : 'address'},
      { 'data' : 'full_name'},
      { 'data' : 'email'},
      { 'data' : 'web_address'},
      { 'data' : 'products'},
      { 'data' : 'products_groups'},
  ]

  jQuery(document).ready(() => {
      let table = jQuery('#workStates').DataTable({
          pageLength: 25,
          lengthMenu: [25, 50, 100, 200],
          order: [[ 1, "desc" ]],
          deferRender: true,
          colResize: true,
          autoWidth: false,
          scrollX: false,
          searching: true,
          responsive: false,
          processing: true,
          serverSide: true,
          searching: false,
          ajax: {
              url: "{{ route('get-all-supplier') }}",
              datatype: 'json',
              type: 'GET'
          },
          columns: columns
      })
  })

  // call search methode
  jQuery('#searchButton').click(() => {
      let searchData = {
          name: jQuery('#name').val(),
          address: jQuery('#address').val(),
          full_name: jQuery('#full_name').val(),
          web_address: jQuery('#web_address').val(),
          email: jQuery('#email').val(),
          products: jQuery('#products').val(),
          products_groups: jQuery('#products_groups').val(),
      }
      jQuery('#workStates').DataTable().destroy()

      let table = jQuery('#workStates').DataTable({
          pageLength: 25,
          lengthMenu: [25, 50, 100, 200],
          order: [[ 1, "desc" ]],
          deferRender: true,
          colResize: true,
          autoWidth: false,
          scrollX: false,
          searching: true,
          responsive: false,
          processing: true,
          serverSide: true,
          searching: false,
          ajax: {
              url: "{{ route('get-all-supplier') }}",
              datatype: 'json',
              type: 'GET',
              data: {
                  searchValues : searchData
              }
          },
          columns: columns
      })
  })

  // copy all emails to clipboard
  jQuery('#copyEmail').click(() => {
      let searchData = {
          name: jQuery('#name').val(),
          address: jQuery('#address').val(),
          full_name: jQuery('#full_name').val(),
          web_address: jQuery('#web_address').val(),
          email: jQuery('#email').val(),
          products: jQuery('#products').val(),
          products_groups: jQuery('#products_groups').val(),
      }


      jQuery.ajax({
          url: "{{ route('all-email-addresses') }}",
          datatype: 'json',
          type: 'GET',
          data: {
              searchValues : searchData
          }
      }).done((data) => {
          jQuery('#all-emails').attr('value', data.emails)

          var copyText = document.getElementById("all-emails");
          copyText.select();
          document.execCommand("copy");

          if(copyText.value === '') {
              alert('there is nothing to copy')
          } else {
              alert('email addresses have been copied')
          }
      })
  })

</script>