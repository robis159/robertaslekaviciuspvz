@extends('crm.layout.template')

@section('stylesheets')
    <link  rel="stylesheet" href="{{ asset('dist/css/lib/datatable/dataTables.bootstrap.min.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.1/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fh-3.1.4/r-2.2.2/sc-1.5.0/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/dataTables.colResize.css') }}" />

    <style>
        .cypy-input {
            border: 1px solid transparent; 
            color: transparent;
            text-decoration: underline dotted transparent;
        }

        .cypy-input::selection {
            background: transparent
        }

        hr {
            margin-top: 0;
        }
    </style>
@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">All suppliers</strong>
                    </div>
                    <div class="card-body">

                        {{-- Let user go back easily, if he doesnt want to create new state --}}
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="{{ route('suppliersDb.create') }}" class="btn btn-info" style="margin-bottom: 20px;">Create new supplier</a>
                            </div>
                        </div>
                            <strong class="card-title">Advanced Search</strong>
                            <hr>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Name', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('name', null, array('class' => 'form-control input-default', 'placeholder' => 'Name')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('address', 'Address', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('address', null, array('class' => 'form-control input-default', 'placeholder' => 'Address')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('full_name', 'Full Name', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('full_name', null, array('class' => 'form-control input-default', 'placeholder' => 'Full Name')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('email', 'Email', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('email', null, array('class' => 'form-control input-default', 'placeholder' => 'Email')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('web_address', 'Web address', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('web_address', null, array('class' => 'form-control input-default', 'placeholder' => 'Web address')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('products', 'Products', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('products', null, array('class' => 'form-control input-default', 'placeholder' => 'Products')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('products_groups', 'Products groups', array('class' => 'mb-0 lb-sm')) !!}
                                            {!! Form::text('products_groups', null, array('class' => 'form-control input-default', 'placeholder' => 'Products groups')) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="height: 50px;">
                                    <div class="col-md-6" id="search">
                                        {!! Form::button('Search', ['id' => 'searchButton', 'type' => 'button','class' => 'btn btn-info' , 'style' => 'margin-bottom: 20px; color: white;']) !!}
                                    </div>
                                    {{-- copy emails to clipboard --}}
                                    <div class="col-md-6" id="search">
                                        <div class="col-sm-9"></div>
                                            {!! Form::button('Copy all email', ['id' => 'copyEmail', 'type' => 'button','class' => 'btn btn-info col-sm-3' , 'style' => 'margin-bottom: 20px; color: white;']) !!}
                                            {!! Form::text('copy-emails', null, ['id' => 'all-emails', 'class' => 'cypy-input']) !!}
                                    </div>
                                </div>
                            <hr>
                        {{-- Show messages when created, updated, deleted --}}
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                            <p class="alert alert-{{ $msg }}">
                                {{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                            @endif
                            @endforeach
                        </div>

                        {{-- If no senders exists yet, show some message, or button to create --}}
                        @if($suppliersCount > 0 || !empty($search))
                        <table class="table table-hover headings" id="workStates" width="100%">
                            <thead>
                                <tr>
                                    <th data-col="action" scope="col">Actions</th>
                                    <th data-col="id" scope="col row-id">#</th>
                                    <th data-col="name" scope="col row-name">Name</th>
                                    <th data-col="address" scope="col row-address">Address</th>
                                    <th data-col="full_name" scope="col row-full_name">Full name</th>
                                    <th data-col="email" scope="col row-email">Email</th>
                                    <th data-col="web_address" scope="col row-web_address">Web address</th>
                                    <th data-col="products" scope="col roe-products">Products</th>
                                    <th data-col="products_groups" scope="col roe-products_groups">Products groups</th>
                                </tr>
                            </thead>
                            <tbody id="list">
                            </tbody>
                        </table>
                        @else
                            <tr>
                                No suppliers added yet!
                            </tr>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.1/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fh-3.1.4/r-2.2.2/sc-1.5.0/datatables.min.js"></script>

    <script src="https://cdn.datatables.net/colreorder/1.5.0/js/dataTables.colReorder.min.js"></script>

    <script src="{{ asset('dist/js/dataTables.colResize.js') }}"></script>

    @include('crm/suppliersDb/script/supplayersDbScript')
@endsection
