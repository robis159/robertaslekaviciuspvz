@extends('crm.layout.template')

@section('stylesheets')

@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Delete a supplier - {{ $supplier->name }} ?</strong>
                    </div>
                    <div class="card-body">

                        {!! Form::open([
                                'route' => ['suppliersBase.destroy', $supplier->id],
                                'method' => 'DELETE',
                                'style' => 'display:inline-block'
                            ]) !!}

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                    &nbsp;Delete
                                </button>
                                <a href="{{ route('get-all-suppliers') }}" class="btn btn-info">Go back</a>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
