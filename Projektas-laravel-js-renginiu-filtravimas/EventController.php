<?php

namespace app\Http\Controllers\Page\Events;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;

// Events
use app\Models\Events\Event;
use app\Models\Events\EventCategory;
use app\Models\Events\EventRegion;

// General models
use app\Models\General\PageSettings;
use app\Models\General\Partner;

class EventController extends Controller
{
    public function event($category, $slug)
    {
        $category = EventCategory::where('slug', $category)->first();
        $event = Event::where('category_id', $category->id)->where('slug', $slug)->first();

        return view('web.events.single-event')->with([
            'page' => PageSettings::first(),
            'partners' => Partner::all(),
            'simEvents' => Event::confirmed()->where('category_id', $event->category_id)->where('id', '!=', $event->id)->limit(4)->get(),
            'event' => $event
        ]);
    }

    public function events()
    {
        return view('web.events.events')->with([
            'page' => PageSettings::first(),
            'partners' => Partner::all(),
            'categories' => EventCategory::all(),
            'events' => Event::confirmed()->get(),
            'regions' => EventRegion::all()
        ]);
    }

    public function eventsByCategory($categorySlug)
    {
        if ($categorySlug !== 'all') {
            $category = EventCategory::where('slug', $categorySlug)->first();
            $events = Event::confirmed()->where('category_id', $category->id)->latest()->get();
        } else {
            $events = Event::confirmed()->get();
        }


        if (!empty($events)) {
            return view('web.events.partials.events-list')->with([
          'events' => $events
        ]);
        } else {
            return response()->json([
          'status' => 'Error',
          'message' => 'No events'
        ], 409);
        }
    }

    // public function filteredEvents($type, $data)
    public function filteredEvents(Request $request)
    {

        $data = [];

        if (isset($request->filter['region'])) {
            $region = EventRegion::where('slug', $request->filter['region'])->get();
            if($request->filter['region'] !== 'all') {
                array_push($data, ['region_id', 'LIKE', '%' . $region[0]['id'] . '%']);
            }
        }

        if (isset($request->filter['category'])) {
            $category = EventCategory::where('slug', $request->filter['category'])->get();
            if ($request->filter['category'] !== 'all') {
                array_push($data, ['category_id', 'LIKE', '%' . $category[0]['id'] . '%']);
            }
        }

        if ($request->filter['free'] === 'true') {
            array_push($data, ['is_free', 'LIKE', '%' . 1 . '%']);
        }

        if (isset($request->filter['date'])) {
            array_push($data, ['date', 'LIKE', '%' . $request->filter['date'] . '%']);
        }

        if (isset($request->filter['name'])) {
            array_push($data, ['title', 'LIKE', '%' . $request->filter['name'] . '%']);
        }

        if (count($data) > 0) {
            $events = Event::where($data)->get();
        } else {
            $events = Event::all();
        }
        \Log::debug('events count' . count($events));

        if (!empty($events)) {
            return view('web.events.partials.events-list')->with([
          'events' => $events
        ]);
        } else {
            return response()->json([
          'status' => 'Error',
          'message' => 'No events'
        ], 409);
        }
    }

    protected function getAllEvents()
    {
        $today = Carbon::now();

        $events = Event::confirmed()->latest()->get();
        $recomendedEvents = Event::confirmed()->recommended()->get();

        $filteredEvents = [];

        /**
         * @todo filter events by same or after today
         */

        if (count($recomendedEvents) > 0) {
            foreach ($recomendedEvents as $event) {
                if (count($filteredEvents) === self::SHOW_EVENTS) {
                    break;
                }

                array_push($filteredEvents, $event);
            }
        }

        foreach ($events as $event) {
            if (count($filteredEvents) === self::SHOW_EVENTS) {
                break;
            }

            array_push($filteredEvents, $event);
        }

        return $filteredEvents;
    }
}
